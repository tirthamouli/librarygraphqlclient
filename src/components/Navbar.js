import React from "react";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <nav>
      <div className="container">
        <a href="/" className="brand-logo">
          Library
        </a>
        <ul className="right hide-on-med-and-down">
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/add">Add Story</Link>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;
