import React, { Component } from "react";
import { graphql } from "react-apollo";
import { getBookQuery } from "../queries/queries";
import BookList from "./BookList";

export class BookDetails extends Component {
  render() {
    const { data } = this.props;
    const details = data.loading ? (
      <div className="card-content">
        <span className="card-title">Loading</span>
      </div>
    ) : data.book ? (
      <div className="card-content">
        <span className="card-title">{data.book.name}</span>
        <p>
          <strong>Genre: </strong>
          {data.book.genre}
        </p>
        <p>
          <strong>Author: </strong>
          {data.book.author.name}
        </p>
      </div>
    ) : null;

    const booksByAuthor = data.loading
      ? false
      : data.book
        ? data.book.author.books
        : false;

    return (
      <div>
        <div className="row">
          <div className="col s8 m6 l4 offset-s2 offset-m3 offset-l4">
            <div className="card">{details}</div>
          </div>
        </div>
        <BookList nested="true" booksByAuthor={booksByAuthor} />
      </div>
    );
  }
}

export default graphql(getBookQuery, {
  options: props => {
    return {
      variables: {
        id: props.match.params.id
      }
    };
  }
})(BookDetails);
