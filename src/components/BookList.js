import React, { Component } from "react";
import { Link } from "react-router-dom";
import { graphql } from "react-apollo";
import { getBooksQuery } from "../queries/queries";

class BookList extends Component {
  render() {
    const bookList = this.props.nested ? (
      this.props.booksByAuthor ? (
        this.props.booksByAuthor.map(book => {
          return (
            <li className="collection-item" key={book.id}>
              <Link to={"/details/" + book.id}>{book.name}</Link>
            </li>
          );
        })
      ) : null
    ) : this.props.data.loading ? (
      <div className="collection-item">Loading</div>
    ) : (
      this.props.data.books.map(book => {
        return (
          <li className="collection-item" key={book.id}>
            <Link to={"/details/" + book.id}>{book.name}</Link>
          </li>
        );
      })
    );
    return (
      <div className="row">
        <ul
          className="collection with-header col s10 m8 l6 offset-s1 offset-m2 offset-l3"
          id="book-list"
        >
          <li className="collection-header">
            <h4>Book List</h4>
          </li>
          {bookList}
        </ul>
      </div>
    );
  }
}

export default graphql(getBooksQuery)(BookList);
