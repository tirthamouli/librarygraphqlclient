import React, { Component } from "react";
import { graphql, compose } from "react-apollo";
import {
  getAuthorQuery,
  addBookMutation,
  getBooksQuery
} from "../queries/queries";

export class AddBook extends Component {
  state = {
    book: "",
    genre: "",
    authorId: 0
  };

  // When something is input
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  // When ever author name is changed
  handleAuthor = e => {
    this.setState({ authorId: e.target.value });
  };

  //handling submit event
  handleSubmit = e => {
    e.preventDefault();
    e.stopPropagation();
    this.props.addBookMutation({
      variables: {
        name: this.state.book,
        genre: this.state.genre,
        authorId: this.state.authorId
      },
      refetchQueries: [{ query: getBooksQuery }]
    });
    this.props.history.push("/");
  };

  // Setting up
  componentDidMount() {
    var elems = document.querySelectorAll("select");
    window.M.FormSelect.init(elems);
  }
  render() {
    const authorList = this.props.getAuthorQuery.loading
      ? null
      : this.props.getAuthorQuery.authors.map(author => {
          return (
            <option key={author.id} value={author.id}>
              {author.name}
            </option>
          );
        });
    return (
      <div className="container">
        <div className="row">
          <div className="card col s10 m8 l6 offset-s1 offset-m2 offset-l3">
            <div className="card-content">
              <form onSubmit={this.handleSubmit}>
                <span className="card-title">Add book...</span>
                <div className="input-field">
                  <input
                    type="text"
                    name="book"
                    id="book"
                    value={this.state.book}
                    onChange={this.handleChange}
                    autoComplete="off"
                  />
                  <label htmlFor="book">Book Name</label>
                </div>
                <div className="input-field">
                  <input
                    type="text"
                    name="genre"
                    id="genre"
                    value={this.state.genre}
                    onChange={this.handleChange}
                  />
                  <label htmlFor="genre">Genre</label>
                </div>
                <div className="input-field">
                  <select
                    className="browser-default"
                    value={this.state.authorId}
                    onChange={this.handleAuthor}
                  >
                    <option value="0" disabled>
                      Choose an author
                    </option>
                    {authorList}
                  </select>
                </div>
                <div className="input-field">
                  <button className="btn">Add</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default compose(
  graphql(getAuthorQuery, { name: "getAuthorQuery" }),
  graphql(addBookMutation, { name: "addBookMutation" })
)(AddBook);
