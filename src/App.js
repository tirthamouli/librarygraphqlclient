import React, { Component } from "react";
import BookList from "./components/BookList";
import Navbar from "./components/Navbar";
import AddBook from "./components/AddBook";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import BookDetails from "./components/BookDetails";

import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";

const client = new ApolloClient({ uri: "/graphql" });

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <ApolloProvider client={client}>
          <div className="App">
            <Navbar />
            <Switch>
              <Route path="/add" component={AddBook} />
              <Route path="/details/:id" component={BookDetails} />
              <Route path="/" component={BookList} />
            </Switch>
          </div>
        </ApolloProvider>
      </BrowserRouter>
    );
  }
}

export default App;
